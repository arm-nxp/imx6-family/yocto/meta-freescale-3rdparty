# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright 2018 (C) O.S. Systems Software LTDA.
# Copyright 2017-2022 NXP
# Copyright 2023 congatec GmbH

require recipes-bsp/u-boot/u-boot.inc

LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://Licenses/gpl-2.0.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"


SUMMARY = "U-Boot for congatec QMX6 boards"
DESCRIPTION = "Bootloader based on NXP u-boot for conga-QMX6 boards."

PROVIDES += "u-boot"

UBOOT_SRC = "git://git.congatec.com/arm-nxp/imx6-family/uboot-imx6-family.git;protocol=https"
SRCBRANCH = "cgtimx6__lf_v2022.04"
SRC_URI = "${UBOOT_SRC};branch=${SRCBRANCH}"
SRCREV = "fade2dacbba69ef26889a5e5192c77816c31444f"


LOCALVERSION = "-yocto"

DEPENDS += "flex-native bison-native bc-native dtc-native gnutls-native"

S = "${WORKDIR}/git"
B = "${WORKDIR}/build"

inherit fsl-u-boot-localversion

BOOT_TOOLS = "imx-boot-tools"


###############################################################
# require recipes-bsp/u-boot/u-boot-imx-common_${PV}.inc



PROVIDES += "u-boot"

inherit uuu_bootloader_tag

UUU_BOOTLOADER        = "${UBOOT_BINARY}"
UUU_BOOTLOADER_TAGGED = "u-boot-tagged.${UBOOT_SUFFIX}"



# Also install combined SPL+uboot binary
# to use with uuu tool from NXP
CGT_SPL_UBOOT_PACKAGED = "u-boot-with-spl.imx"
CGT_SPL_UBOOT_MACHINE = "${@d.getVar('UBOOT_MACHINE').strip()}"
CGT_SPL_UBOOT_CONFIG = "${@d.getVar('UBOOT_CONFIG').strip()}"
FILES:${PN} += "${CGT_SPL_UBOOT_PACKAGED}"

deploy_tag() {
    # Copy combined SPL+uboot binary to deploy folder
    install -D -m 644 ${D}/${CGT_SPL_UBOOT_PACKAGED} ${DEPLOY_DIR_IMAGE}/${CGT_SPL_UBOOT_PACKAGED}
    ln -sf ${CGT_SPL_UBOOT_PACKAGED} ${CGT_SPL_UBOOT_PACKAGED}-${MACHINE}-${CGT_SPL_UBOOT_CONFIG}-${PV}-${PR}
}

install_spl_uboot_packaged() {
    # Also install combined SPL+uboot binary
    install -D -m 644 ${B}/${CGT_SPL_UBOOT_MACHINE}/${CGT_SPL_UBOOT_PACKAGED} ${D}/${CGT_SPL_UBOOT_PACKAGED}
}


do_deploy:append() {
    deploy_tag
}
do_install:append() {
    install_spl_uboot_packaged
}

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "cgtqmx6"

UBOOT_NAME = "u-boot-${MACHINE}.bin-${UBOOT_CONFIG}"
