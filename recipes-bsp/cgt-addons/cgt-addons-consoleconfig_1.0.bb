# SPDX-License-Identifier: GPL-2.0
# Copyright 2022 congatec GmbH

# Configure the (serial-)console

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"


SRC_URI = " file://cgt-consoleconfig.sh"


do_install() {
    install -d ${D}${sysconfdir}/profile.d/
    install -m 0755 ${WORKDIR}/cgt-consoleconfig.sh ${D}${sysconfdir}/profile.d/
}
