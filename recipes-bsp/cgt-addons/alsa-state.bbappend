# SPDX-License-Identifier: GPL-2.0
# Copyright 2022 congatec GmbH

# Set first audio card to default output for asound

COMPATIBLE_MACHINE = "cgtqmx6"

do_install:append() {

    sed -i '1s/^/defaults.pcm.card 0\ndefaults.ctl.card 0\n\n/' ${D}${sysconfdir}/asound.conf
}
