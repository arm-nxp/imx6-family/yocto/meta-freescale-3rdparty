# SPDX-License-Identifier: GPL-2.0
# Copyright 2022 congatec GmbH

# set console width and height
stty cols 150 rows 50

# enable colors in ls command
alias ls='ls --color=auto'


