# Copyright (C) 2013-2016 Freescale Semiconductor
# Copyright 2017-2022 NXP
# Copyright 2022 congatec GmbH
# Released under the MIT license (see COPYING.MIT for the terms)
#
# SPDX-License-Identifier: MIT
#

SUMMARY = "Linux Kernel for congatec QMX6 boards"
DESCRIPTION = "Linux Kernel based on NXP kernel for conga-QMX6 boards. \
It includes support for many IPs such as GPU, VPU and IPU."

require recipes-kernel/linux/linux-imx.inc

LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

DEPENDS += "lzop-native bc-native"


SRCBRANCH = "cgtimx6__lf-5.15.32-2.0.0"

KERNEL_SRC = "git://git.congatec.com/arm-nxp/imx6-family/kernel-imx6-family.git;protocol=https"
KBRANCH = "${SRCBRANCH}"
SRC_URI = "${KERNEL_SRC};branch=${SRCBRANCH}"

SRCREV = "1ab1429133a593fa75ffaacf025c90729e76f2e8"

# LOCALVERSION = "-yocto"

# PV is defined in the base in linux-imx.inc file and uses the LINUX_VERSION definition
# required by kernel-yocto.bbclass.
#
# LINUX_VERSION define should match to the kernel version referenced by SRC_URI and
# should be updated once patchlevel is merged.
LINUX_VERSION = "5.15.32"

KERNEL_CONFIG_COMMAND = "oe_runmake_call -C ${S} CC="${KERNEL_CC}" O=${B} olddefconfig"

DEFAULT_PREFERENCE = "1"

KBUILD_DEFCONFIG = "cgtqmx6_defconfig"

KERNEL_VERSION_SANITY_SKIP="1"
COMPATIBLE_MACHINE = "cgtqmx6"
